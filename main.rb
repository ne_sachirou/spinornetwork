# encoding = utf-8

require 'sinatra'
require 'erubis'

$PAGES_INFO = {
  index: {title: 'SpinorNetwork'},
  members: {title: 'Residents'},
  works: {title: 'Past Works'},
  working: {title: 'Crain working on SpnrNtwrk'},
  notfound: {title: '404 Not Found'}
}

$HEADER_NAV = [:index, :working, :members, :works]


helpers do
  def get_title contents_name
    $PAGES_INFO[contents_name][:title]
  end


  def gen_header_nav contents_name
    str = ''
    $HEADER_NAV.each do |item|
      str += '<div class="item">'
      str += '<a href="/' +
        (item == :index ? '' : item.to_s) +
        '">' + $PAGES_INFO[item][:title] + '</a>'
      str += '</div>'
    end
    str
  end


  def gen_audio_element source_url
    "<audio controls><source type=\"audio/ogg\" codecs=\"vorbis\" src=\"#{source_url}.ogg\" /><source type=\"audio/mpeg\" src=\"#{source_url}.mp3\" /></audio>"
  end
end


not_found do
  status 404
  @contents_name = :notfound
  erb :template
end


get '/' do
  @contents_name = :index
  erb :template
end


get %r{/game/(?:index(?:\.html?)?)?} do
  send_file './public/game/Sp2game.html'
end


get %r{/([^./]+)(?:\.html?)?} do
  @contents_name = :"#{params[:captures][0]}"
  if !($PAGES_INFO.has_key? @contents_name)
    not_found
  else
    erb :template
  end
  #if (Rack::Utils.parse_query request.query_string)['_pjax']
  #  erubis @contents
  #else
  #  erubis :index
  #end
end
