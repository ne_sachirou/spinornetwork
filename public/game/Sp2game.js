/**
 * @fileOverview A game for downloading SpinorNetwork2.
 * @author ne_Sachirou
 * @version 2012/06/17
 * @license MIT License
 */

(function (_document, _window) {

/*========================================
JavaScript Canvas like Processing.
========================================*/
var canvas = _document.getElementById('game'),
    context = canvas.getContext('2d'),
    width = 0,
    height = 0,
    fill = null, // {String} fillStyle color
    stroke = null, // {String} strokeStyle color
    alpha = 1, // {Number} globalAlpha
    strokeWidth = 1, // {Number} lineWidth
    strokeCap = 'butt', // {'butt'|'round'|'square'} lineCap
    strokeJoin = 'miter', // {'round'|'bevel'|'miter'} lineJoin
    mouseX = 0,
    mouseY = 0,
    pmouseX = 0, // previous mouseX
    pmouseY = 0; // previous mouseY


/**
 * Set global mouseX, mouseY, pmouseX, pmouseY variable.
 * @param {MouseEvent} evt MouseMoveEvent Object
 */
function _getMousePoint (evt) {
  var rect;

  pmouseX = mouseX;
  pmouseY = mouseY;
  if (evt.offsetX) {
    mouseX = evt.offsetX;
    mouseY = evt.offsetY;
  } else if (evt.layerX) {
    mouseX = evt.layerX;
    mouseY = evt.layerY;
  } else {
    rect = evt.target.getBoundingClientRect();
    mouseX = evt.clientX - rect.left;
    mouseY = evt.clientY - rect.top;
  }
}
canvas.addEventListener('mousemove', _getMousePoint, false);
canvas.addEventListener('click', _getMousePoint, false);
canvas.addEventListener('mouseout', function (evt) {
  pmouseX = mouseX;
  pmouseY = mouseY;
}, false);


/**
 * Set Canvas context properties by global Processing like variables.
 */
function adjustEnvironment () {
  if (fill) {context.fillStyle = fill;}
  if (stroke) {context.strokeStyle = stroke;}
  if (alpha < 0) {alpha = 0;}
  context.globalAlpha = alpha - 0;
  if (strokeWidth < 0) {strokeWidth = 0;}
  context.lineWidth = strokeWidth - 0;
  context.lineCap = strokeCap || 'butt';
  context.lineJoin = strokeJoin || 'miter';
}


/**
 * @param {Function} callbask
 * @return {Function} Stop animation function.
 */
function animate (callback) {
  var timerID, stop, requestAnimationFrame, cancelRequestAnimationFrame;

  function animationFun () {
    adjustEnvironment();
    context.clearRect(0, 0, width, height);
    callback();
    if (requestAnimationFrame) {
       requestAnimationFrame(animationFun);
    }
  }

  if (_window.requestAnimationFrame && _window.cancelAnimationFrame) {
    requestAnimationFrame = _window.requestAnimationFrame;
    cancelAnimationFrame = _window.cancelAnimationFrame;
  } else if (_window.mozRequestAnimationFrame && _window.mozCancelAnimationFrame) {
    requestAnimationFrame = _window.mozRequestAnimationFrame;
    cancelAnimationFrame = _window.mozCancelAnimationFrame;
  } else if (_window.msRequestAnimationFrame && _window.msCancelAnimationFrame) {
    requestAnimationFrame = _window.msRequestAnimationFrame;
    cancelAnimationFrame = _window.msCancelAnimationFrame;
  } else if (_window.webkitRequestAnimationFrame && _window.webkitCancelAnimationFrame) {
    requestAnimationFrame = _window.webkitRequestAnimationFrame;
    cancelAnimationFrame = _window.webkitCancelAnimationFrame;
  } else if (_window.oRequestAnimationFrame && _window.oCancelAnimationFrame) {
    requestAnimationFrame = _window.oRequestAnimationFrame;
    cancelAnimationFrame = _window.oCancelAnimationFrame;
  }

  if (requestAnimationFrame) {
    timerID = requestAnimationFrame(animationFun);
    stop = function () {
      cancelAnimationFrame(timerID);
    };
  } else {
    timerID = setInterval(animationFun, 16);
    stop = function () {
      clearInterval(timerID);
    };
  }
  return stop;
}


/**
 * @param {Number} w
 * @param {Number} h
 */
function size (w, h) {
  canvas.width = w;
  canvas.height = h;
  width = w;
  height = h;
}


/*========================================
Game object like enchant.js.
========================================*/
/**
 * @class Game
 * @property {Array} preloadImages { get, set }
 * @property {Hash} preloadAudios { get, set }
 * @property {Array} images { get }
 * @property {Array} audioPlayers { get }
 * @property {Scene} currentScene { get }
 * @property {Function} onload { get, set } Callback when the game was loaded.
 */
function Game () {
  var _me = this;

  if (!(_me instanceof Game)) { return new Game(); }
  _me.preloadImages = [];
  _me.preloadAudios = {};
  _me.images = {};
  _me.audioPlayers = [];
}


Game.prototype = {
  start: function () {
    var timerID, i = len = 0, tmp,
        _me = this,
        restNumberOfLoadedItems = 0;

    function uniteCallback (evt, player, trackNumber, currentTime) {
      switch (evt.type) {
      case 'canplay': case 'abort':
        --restNumberOfLoadedItems;
        break;
      default: break;
      }
    }

    for (i = 0, len = _me.preloadImages.length; i < len; ++i) {
      ++restNumberOfLoadedItems;
      tmp = new Image();
      tmp.src = _me.preloadImages[i];
      tmp.onload = function () { --restNumberOfLoadedItems; };
      _me.images[_me.preloadImages[i]] = tmp;
    }
    restNumberOfLoadedItems += 2;
    _me.audioPlayers[0] = new UnitePlayer(_me.preloadAudios, 0, uniteCallback);
    _me.audioPlayers[1] = new UnitePlayer(_me.preloadAudios, 1, uniteCallback);
    timerID = setInterval(function () {
      if (restNumberOfLoadedItems <= 0) {
        clearInterval(timerID);
        _me.onload();
       }
    }, 30);
  },


  /**
   * @param {Scene} scene
   */
  loadScene: function (scene) {
    var _me = this;

    if (_me.currentScene) { _me.currentScene.exit(); }
    _me.currentScene = scene;
    scene.start();
  },


  exit: function () {
    if (this.currentScene) { this.currentScene.exit(); }
  }
};


/**
 * @class Scene
 * @property {Function} onload { get, set } Callback when the scene was loaded. This is the scene main procedure.
 * @property {Function} after { get, set } Callback before the next scene was loaded.
 */
function Scene () {
  if (!(this instanceof Scene)) { return new Scene(); }
}


Scene.prototype = {
  start: function () {
    this.onload();
  },


  exit: function () {
    if (this.after) { this.after(); }
  }
};


/**
 * @class Sprite
 * @param {Image} image
 * @param {Number} startX
 * @param {Number} startY
 * @param {Number} spriteWidth
 * @param {Number} spriteHeight
 * @property {Image} image { get }
 * @property {Array} startPoint { get } [startX, startY]
 * @property {Array} splitSize { get } [spriteWidth, spriteHeight]
 */
function Sprite (image, startX, startY, spriteWidth, spriteHeight) {
  var _me = this;

  if (!(_me instanceof Sprite)) {
    return new Sprite(image, startX, startY, spriteWidth, spriteHeight);
  }
  _me.image = image;
  _me.startPoint = [startX, startY];
  _me.spriteSize = [spriteWidth, spriteHeight];
}


Sprite.prototype = {
  /**
   * @param {CanvasContext} context
   * @param {Number} drawX
   * @param {Number} drawY
   * @param {Number} drawWidth (=this.spriteSize[0])
   * @param {Number} drawHeight (=this.spriteSize[1])
   */
  draw: function (context, drawX, drawY, drawWidth, drawHeight) {
    var _me = this;

    if (drawWidth === void 0) { drawWidth = _me.spriteSize[0]; }
    if (drawHeight === void 0) { drawHeight = _me.spriteSize[1]; }
    context.drawImage(_me.image,
      _me.startPoint[0], _me.startPoint[1], _me.spriteSize[0], _me.spriteSize[1],
      drawX, drawY, drawWidth, drawHeight);
  }
};


/*========================================
Main user code.
========================================*/
var game, firstScene, answerScene;


firstScene = new Scene();
firstScene.momongas = [];
firstScene.stop = null;

/**
 * @class firstScene.Momonga
 * @param {Sprite} spriteImage
 * @param {Hash} param
 * @property {Split} image { get }
 * @property {Hash} param { get }
 * @property {Function} before { get, set } Callback before drawing.
 * @property {Function} after { get, set } Callback after drawing.
 * @property {Function} onclick { get, set } Callback when the Momonga was clicked.
 * @property {Hash} _cur
 */
firstScene.Momonga = function (spriteImage, param) {
  var _me = this;

  if (!(_me instanceof firstScene.Momonga)) {
    return new firstScene.Momonga(spriteImage, param);
  }
  _me.image = spriteImage;
  _me.param = param;
  _me._cur = {x: param.x,
               diff: param.amp / 2 / param.t};
};


firstScene.Momonga.prototype = {
  draw: function () {
    var _me = this, _param = _me.param, _cur = _me._cur;

    if (_me.before) { _me.before(); }
    _me.image.draw(context, _cur.x, _param.y, _param.w, _param.h);
    if (_me.after) { _me.after(); }
    if (_cur.x + _cur.diff > _param.x + _param.amp / 2 ||
        _cur.x + _cur.diff < _param.x - _param.amp / 2) {
      _cur.diff *= -1;
    }
    _cur.x += _cur.diff;
  },


  clicked: function () {
    game.audioPlayers[1].preset('momongaCry');
    if (this.onclick) { this.onclick(); }
  },


  blink: function () {
    var _me = this, _param = _me.param, _cur = _me._cur;

    _cur._a = 1;
    _cur._diffA = -0.01;
    _me.before = function () {
      if (_cur._a <= 0.3) { _cur._diffA *= -1; }
      _cur._a += _cur._diffA;
      context.globalAlpha = _cur._a;
      if (_cur._a >= 1) { _me.before = _me.after = void 0; }
    };
    _me.after = function () {
      context.globalAlpha = 1;
    };
  },


  bounce: function () {
    var _me = this, _param = _me.param, _cur = _me._cur,
        originalY = _param.y;

    if (_cur._direction !== void 0) { return; }
    _cur._direction = 1;
    _me.before = function () {
      switch (_cur._direction) {
      case 1:
        _param.y += 0.2;
        if (_param.y >= originalY + 5) { _cur._direction = 2; }
        break;
      case 2:
        _param.y -= 0.2;
        if (_param.y <= originalY - 5) { _cur._direction = 3; }
        break;
      case 3:
        _param.y += 0.2;
        if (_param.y >= originalY) {
          _param.y = originalY;
          _cur._direction = void 0;
          _me.before = void 0;
        }
        break;
      default: break;
      }
    };
  },


  swing: function () {
    /** @const */
    var RAD = 3.141592653589793 / 100;

    var _me = this, _param = _me.param, _cur = _me._cur,
        rot = 0;

    if (_cur._direction !== void 0) { return; }
    _cur._direction = 1;
    _me.before = function () {
      switch (_cur._direction) {
      case 1:
        rot += 0.05 * RAD;
        if (rot >= 1 * RAD) { _cur._direction = 2; }
        break;
      case 2:
        rot -= 0.05 * RAD;
        if (rot <= -1 * RAD) { _cur._direction = 3; }
        break;
      case 3:
        rot += 0.05 * RAD;
        if (rot >= 0) {
          rot = 0;
          _cur._direction = void 0;
          _me.before = _me.after = void 0;
        }
        break;
      default: break;
      }
      context.rotate(rot);
    };
    _me.after = function () {
      context.rotate(-rot);
    };
  }
};


firstScene.detectClick = function (evt) {
  var momonga,
      i = -1;

  while (momonga = firstScene.momongas[++i]) {
    momongaImage = momonga.image;
    if ((momonga._cur.x <= mouseX && mouseX <= momonga._cur.x + momongaImage.spriteSize[0]) &&
        (momonga.param.y <= mouseY && mouseY <= momonga.param.y + momongaImage.spriteSize[1])) {
      momonga.clicked();
      break;
    }
  }
};


firstScene.onload = function () {
  var momongaImages = [
    new Sprite(game.images['/game/momonga.png'], 0, 0, 36, 74),
    new Sprite(game.images['/game/momonga.png'], 36, 0, 36, 74),
    new Sprite(game.images['/game/momonga.png'], 72, 0, 36, 74),
    new Sprite(game.images['/game/momonga.png'], 108, 0, 36, 74)];

  function resistMomonga(imageNum, param, onclickCallback) {
    var momonga = new firstScene.Momonga(momongaImages[imageNum], param);

    if (typeof onclickCallback === 'string') { momonga.onclick = momonga[onclickCallback]; }
    else { momonga.onclick = onclickCallback; }
    firstScene.momongas.push(momonga);
  }

  resistMomonga(0, { x: 10, y: 10, amp: 10, t: 50 }, 'blink');
  resistMomonga(1, { x: 100, y: 60, amp: 20, t: 40 }, 'swing');
  resistMomonga(2, { x: 50, y: 400, amp: 20, t: 100 },
    function (evt) { game.loadScene(answerScene); });
  resistMomonga(3, { x: 700, y: 100, amp: 30, t: 70 }, 'blink');
  resistMomonga(0, { x: 500, y: 200, w: 24, h: 49, amp: 10, t: 40 }, 'bounce');
  resistMomonga(1, { x: 400, y: 500, w: 24, h: 49, amp: 10, t: 50 }, 'blink');
  resistMomonga(2, { x: 600, y: 300, w: 48, h: 99, amp: 20, t: 30 }, 'bounce');
  resistMomonga(3, { x: 200, y: 160, w: 48, h: 99, amp: 30, t: 70 }, 'blink');
  resistMomonga(0, { x: 560, y: 160, w: 48, h: 99, amp: 20, t: 50 }, 'bounce');
  resistMomonga(1, { x: 300, y: 60, w: 48, h: 99, amp: 10, t: 100 }, 'blink');
  resistMomonga(2, { x: 360, y: 260, w: 24, h: 49, amp: 20, t: 30 }, 'blink');
  resistMomonga(3, { x: 560, y: 460, w: 24, h: 49, amp: 30, t: 70 }, 'bounce');
  game.audioPlayers[0].preset(['KrmAKxlI', 'momongaBG'][Math.floor(Math.random() * 2)]);
  firstScene.stop = animate(function () {
    context.drawImage(game.images['/game/momongaBG.jpg'], 0, 0);
    firstScene.momongas.forEach(function (momonga, idx) { momonga.draw(); });
  });
  canvas.addEventListener('click', firstScene.detectClick, false);
};


firstScene.after = function () {
  firstScene.stop();
  game.audioPlayers[0].pause();
  canvas.removeEventListener('click', firstScene.detectClick);
};


answerScene = new Scene();
answerScene.stop = null;


answerScene.onload = function () {
  var stop, imageData,
      value = 0;

  imageData = context.getImageData(0, 0, width, height);
  answerScene.stop = animate(function () {
    context.putImageData(imageData, 0, 0);
    context.fillStyle = fill = 'rgba(255, 255, 255, $)'.replace('$', value);
    context.fillRect(0, 0, width, height);
    if (value >= 1) { game.exit(); }
    value += 0.01;
  });
};


answerScene.after = function () {
  answerScene.stop();
  canvas.style.display = 'none';
  _document.getElementById('answer').style.display = 'block';
};


size(800, 600);
game = new Game();
game.preloadImages = ['/game/momonga.png', '/game/momongaBG.jpg'];
game.preloadAudios = {
  mp3: '/game/momonga.mp3',
  ogg: '/game/momonga.ogg',
  volume: 0.5,
  offset: 0,
  preset: {
    KrmAKxlI: ['0:15', '3:27', true],
    momongaBG: ['3:33', '3:48.238', true],
    momongaCry: ['3:54', '3:57.806']
  }
};
game.onload = function () { game.loadScene(firstScene); };
game.start();

}(document, window));
/* vim:set fenc=utf-8 expandtab sw=2: */
